import { Template } from 'meteor/templating';
import { TasksCollection } from "../api/TasksCollection";
import './App.html';
import './Task.js';

Template.body.helpers({
  tasks() {
    return TasksCollection.find({}, {sort: {createdAt: -1}});
  },
});

Template.form.events({
    "submit .task-form"(event) {
      
      event.preventDefault();
  
      const target = event.target;
      const text = target.text.value;
  
      Meteor.call('tasks.insert', text);
  
      target.text.value = '';
    }
  })